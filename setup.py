"""
This is a dummy setup.py file.  Please do not edit it!

All this package's information is defined in `setup.cfg`.
"""
import setuptools
setuptools.setup()