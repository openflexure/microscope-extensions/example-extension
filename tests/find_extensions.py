
try:
    from importlib import metadata
except ImportError:
    import importlib_metadata as metadata



def find_extensions():
    try:
        installed_plugins = metadata.entry_points()["openflexure_microscope_extensions"]
    except KeyError:
        print("There do not appear to be any installed plugins.")
        exit()
    for p in installed_plugins:
        try:
            l = p.load()
            if l.configuration_required():
                p.status = "configuration_required"
            else:
                p.status = "loaded"
        except ImportError as e:
            # TODO: think of a method that is less platform-specific!
            if hasattr(e, "of_raspbian_install_script"):
                p.status = "install_script_required"
                p.install_script = e.of_raspbian_install_script
            else:
                p.status = "broken"
            p.import_error = e
    return installed_plugins

if __name__ == "__main__":
    installed_plugins = find_extensions()
    for p in installed_plugins:
        print(f"[{p.status}]: {p.name} (from {p.value})")

    # Find extensions that don't load because they need pre-install scripts to be run
    install_required = [p for p in installed_plugins if p.status == "install_script_required"]
    if len(install_required) > 0:
        print("Some extensions require prerequisites to be installed before they can be loaded:")
        for p in install_required:
            print(f"**{p.name}** (from {p.value}) suggests:")
            print(p.install_script)
            print()

    configuration_required = [p for p in installed_plugins if p.status == "configuration_required"]
    if len(configuration_required) > 0:
        print("Some extensions need you to run their post-install configuration script before they will work:")
        for p in configuration_required:
            print(f"**{p.name}** (from {p.value})")
    
    
