# OFM Example Plugin
This repository contains a Python package.  It provides an example plugin for the OpenFlexure Microscope.

If you want to customise it to create your own plugin, you should:

* Remove the `example-ofm-extension` module from the `src` folder, and replace it with your own Python module
* Update `setup.cfg`, at the minimum specifying a new `openflexure_microscope_extension` entry point and updating the package name and author.

NB this example repository has been created assuming the pending OFEP for extensions is approved.  **It will not work until at least the next release (likely 2.11)** of the microscope server!