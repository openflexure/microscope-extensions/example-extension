import logging
import time

from labthings import fields
from labthings.extensions import BaseExtension
from labthings.views import ActionView


class DevToolsExtension(BaseExtension):
    def __init__(self) -> None:
        super().__init__(
            "org.openflexure.example-ofm-extension",
            version="0.1.0",
            description="An example plugin",
        )
        self.add_view(RaiseException, "/raise")
        self.add_view(SleepFor, "/sleep")

    @classmethod
    def configuration_required(cls):
        """Indicate if `configure_extension` must be run

        If you rely on something being done before your extension
        will work as intended, you can indicate it here.  This
        is intended to take the place of a "post-install" hook for
        the extension.  For example, if you rely on some 
        permissions being set, you should check them here.

        By default, this will return false, so if you don't need
        to add a post-install script, you may omit this method.
        """
        return False

    @classmethod
    def configure_extension(cls):
        """A post-install script to finish installing the extension
        
        Most extensions can be installed simply by installing the
        relevant Python module.  If you need to do more than that, 
        for example adjusting configuration files or updating
        permissions, you should do it here.
        
        This method will not be called automatically, but the user
        will be informed about any extensions where
        `configuration_required()` returns `True` and prompted to
        run `configure_extension()`
        """
        pass

class RaiseException(ActionView):
    def post(self):
        raise Exception("The developer raised an exception")


class SleepFor(ActionView):
    schema = {"TimeAsleep": fields.Float()}
    args = {"time": fields.Float(description="Time to sleep, in seconds", example=0.5)}

    def post(self, args):
        sleep_time: int = args.get("time", 0)
        logging.info("Going to sleep for %s...", sleep_time)
        start = time.time()
        time.sleep(sleep_time)
        end = time.time()
        logging.info("Waking up!")
        return {"TimeAsleep": (end - start)}