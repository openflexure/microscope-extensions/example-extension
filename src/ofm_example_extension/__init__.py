# If your module has dependencies that can't be installed
# with pip (e.g. libraries installed at system level with
# apt), you should indicate this in an exception.
#
# For example, `numpy` on the Pi will throw an exception
# if you try to import it without having first installed
# `libatlas3-base` and `libgfortran5`.
# 
# You can wrap your imports in an exception handler, like
# the one below, to indicate to the OFM extension loader
# that your extension can't currently load, but that it 
# can probably be fixed by running a particular command.
# 
# NB if there are other set-up tasks that need to take
# place before your extension works properly, but they
# don't prevent the module from loading, please use 
# the `configuration_required()` method to indicate this.
try:
    import numpy
except ImportError as e:
    e.of_raspbian_install_script = "sudo apt install libatlas3-base libgfortran5"
    raise e

